<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-5">
			<?php $form = ActiveForm::begin(['action' => Url::toRoute('site/signup'), 'method' => 'post', 'id' => 'form-signup']); ?>
				<p>Пожалуйста, заполните все поля:</p>
				<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
				<?= $form->field($model, 'login')->textInput() ?>
				<?= $form->field($model, 'firm')->textInput() ?>
				<?= $form->field($model, 'password')->passwordInput() ?>
				<?= $form->field($model, 'password_repeat')->passwordInput() ?>
				<div class="form-group">
					<?= Html::submitButton('Зарегистрировать', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
				</div>
			<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>