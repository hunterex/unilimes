<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title					 = 'Редактирование товара: ' . $model->name;
$this->params['breadcrumbs'][]	 = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][]	 = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][]	 = 'Update';
?>
<div class="row">
	 <div class="col-md-6 col-lg-6">
		  <div class="items-update">

			   <h1><?= Html::encode($this->title) ?></h1>

			   <?=
			   $this->render('_form', [
				   'model'		 => $model,
				   'category'	 => $category
			   ])
			   ?>

		  </div>
	 </div>
</div>
