<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title					 = 'Товары';
$this->params['breadcrumbs'][]	 = $this->title;
?>
<div class="row">
	 <div class="col-md-8 col-lg-8">
		  <div class="items-index">

			   <h1><?= Html::encode($this->title) ?></h1>

			   <p>
				   <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
			   </p>

			   <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

			   <?=
			   GridView::widget([
				   'dataProvider'	 => $dataProvider,
				   'filterModel'	 => $searchModel,
				   'columns'		 => [
					   ['class' => 'yii\grid\SerialColumn'],
					   [
						   'attribute' => 'id',
						   'label' => '№ п/п',
						   'filter' => false,
						   ],
					   [
						   'attribute'	 => 'category_id',
						   'label' => 'Категория',
						   'filter'	 => $category_filter,
						   'value'		 => 'category.name'
					   ],
					   'name',
					   ['class' => 'yii\grid\ActionColumn'],
				   ],
			   ]);
			   ?>


		  </div>
	 </div>
</div>