<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title					 = $model->name;
$this->params['breadcrumbs'][]	 = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][]	 = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
	 <div class="col-md-6 col-lg-6">
		  <div class="items-view">

			   <h1><?= Html::encode($this->title) ?></h1>

			   <p>
				   <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
				   <?=
				   Html::a('Удалить', ['delete', 'id' => $model->id], [
					   'class'	 => 'btn btn-danger',
					   'data'	 => [
						   'confirm'	 => 'Are you sure you want to delete this item?',
						   'method'	 => 'post',
					   ],
				   ])
				   ?>
			   </p>

			   <?=
			   DetailView::widget([
				   'model'		 => $model,
				   'attributes' => [
					   [
						   'attribute' => 'id',
						   'label' => '№ п/п'
						],
					   'category.name',
					   'name',
				   ],
			   ])
			   ?>

		  </div>
	 </div>
</div>

