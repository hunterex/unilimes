<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Items */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-form">

	 <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'category_id')->dropDownList($category)->label('Категория') ?>
	
	 <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	 <div class="form-group">
		  <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	 </div>

	 <?php ActiveForm::end(); ?>

</div>