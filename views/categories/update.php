<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */

$this->title					 = 'Редактирование категории: ' . $model->name;
$this->params['breadcrumbs'][]	 = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][]	 = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][]	 = 'Редактирование';
?>
<div class="row">
	 <div class="col-md-6 col-lg-6">
		  <div class="categories-update">
			   <h1><?= Html::encode($this->title) ?></h1>
			   <?=
			   $this->render('_form', [
				   'model' => $model,
			   ])
			   ?>
		  </div>
	 </div>
</div>
