<?php

return [
	'senderEmail'					 => 'noreply@example.com',
	'senderName'					 => 'Example.com mailer',
	'adminEmail'					 => 'ohunterex@gmail.com.com',
	'user.passwordResetTokenExpire'	 => 3600,
	'supportEmail'					 => 'robot@devreadwrite.com',
];
