<?php
//Internal config - do not edit. 
return [
	'class' => 'app\components\MailerH', //path to the class
	'viewPath' => '@app/mail',				   //path to the template 
	'useFileTransport' => false,			   //false - emails will be sent, true - emails will not be sent and will be saved in a local file
];