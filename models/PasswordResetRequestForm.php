<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PasswordResetRequestForm extends Model {

	public $email;

	public function rules() {
		return [
			['email', 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'exist',
				'targetClass'	 => '\app\models\profile',
				'message'		 => 'Нет пользователя с такой почтой.'
			],
		];
	}

	public function sendEmail() {
		$profile = Profile::findOne([
			'email' => $this->email,
		]);

		if ($profile && !User::isPasswordResetTokenValid($profile->user->password_reset_token)) {
			$user = User::findOne($profile->user_id);
			$user->generatePasswordResetToken();
			if ($user->save()) {
				$mail = Yii::$app
				->mailer
				->compose(
				['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user]
				)
				->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->name . ' robot'])
				->setTo($this->email)
				->setSubject('Восстановление пароля');
				return $mail->send();
			}
		}
		return false;
	}

}