<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {

	public static function tableName() {
		return 'user';
	}

	public function rules() {
		return [
			[['login', 'pass', 'authKey',], 'required'],
			[['login', 'pass'], 'string', 'max' => 64],
			[['password_reset_token', 'authKey', 'accessToken'], 'string', 'max' => 64],
			[['login'], 'unique'],
		];
	}

	public function attributeLabels() {
		return [
			'id'					 => 'ID',
			'login'					 => 'Логин',
			'pass'					 => 'Пароль',
			'authKey'				 => 'Auth Key',
			'accessToken'			 => 'Access Token',
			'password_reset_token'	 => 'Reset Token'
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function validateAuthKey($authKey) {
		return $this->getAuthKey() === $authKey;
	}

	public function getProfiles() {
		return $this->hasMany(Profile::className(), ['user_id' => 'id']);
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function getAuthKey() {
		return $this->authKey;
	}

	public static function isPasswordResetTokenValid($token) {

		if (empty($token)) {
			return false;
		}

		$timestamp	 = (int) substr($token, strrpos($token, '_') + 1);
		$expire		 = Yii::$app->params['user.passwordResetTokenExpire'];
		return $timestamp + $expire >= time();
	}

	public function generatePasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	public function removePasswordResetToken() {
		$this->password_reset_token = null;
	}

	public function setPassword($password) {
		$this->pass = Yii::$app->security->generatePasswordHash($password);
	}
	public static function findByPasswordResetToken($token) {

		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}

		return static::findOne([
			'password_reset_token' => $token,
		]);
	}

	public static function create(SignupForm $model) {
		$tr				 = Yii::$app->db->beginTransaction();
		$user			 = new self();
		$user->pass		 = Yii::$app->security->generatePasswordHash($model->password);
		$user->authKey	 = Yii::$app->security->generateRandomString();
		$user->login	 = $model->login;
		if ($user->save()) {
			if (Profile::create($user->id, $model->email, $model->firm)) {
				$tr->commit();
				return $user;
			}
		}
		$tr->rollback();
		return false;
	}

	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->pass);
	}
}