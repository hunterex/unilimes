<?php

namespace app\models;

use app\components\Utils;
use Yii;

class Mailer {

	public static function sendSaccessfullRegistration($login, $password,$email) {
		Yii::$app->mailer->compose()
		->setFrom(Utils::Settings('senderEmail'))
		->setTo(Utils::Settings($email))
		->setSubject('Регистрация на сайте')
		->setTextBody("Вы успешно зарегистрированы на сайте. Ваш логин $login, пароль $password")
		->send();
	}

}