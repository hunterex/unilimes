<?php

namespace app\models;

use yii\base\Model;

class SignupForm extends Model {

	public $login;
	public $password;
	public $password_repeat;
	public $email;
	public $firm;

	public function rules() {
		return [
			['login', 'trim'],
			['login', 'required', 'message' => 'Это поле не может быть пустым'],
			['login', 'string', 'max' => 255],
			['email', 'trim'],
			['email', 'required', 'message' => 'Это поле не может быть пустым'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'unique', 'targetClass' => '\app\models\Profile', 'message' => 'Этот адрес уже используется.'],
			[['firm'], 'required', 'message' => 'Это поле не может быть пустым'],
			[['firm'], 'string'],
			['password', 'required', 'message' => 'Это поле не может быть пустым'],
			['password', 'string', 'min' => 6],
			['password_repeat', 'required', 'message' => 'Это поле не может быть пустым'],
			['password_repeat', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false, 'message' => "Пароли не совпадают"],
		];
	}

	public function attributeLabels() {
		return [
			'email' => 'E-mail',
			'login' => 'Логин',
			'password' => 'Пароль',
			'password_repeat' => 'Повторите пароль',
			'firm' => 'Фирма'
		];
	}

	public function signup() {
		if ($this->validate()) {
			return User::create($this);
		}
	}

}
