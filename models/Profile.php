<?php

namespace app\models;

use yii\db\ActiveRecord;

class Profile extends ActiveRecord {

	public static function tableName() {
		return 'profile';
	}

	public function rules() {
		return [
			[['user_id', 'firm', 'email'], 'required'],
			[['user_id'], 'integer'],
			['firm', 'string', 'max' => 64],
			['firm', 'trim'],
			['firm', 'required', 'message' => 'Это поле не может быть пустым'],
			['email', 'trim'],
			['email', 'required', 'message' => 'Это поле не может быть пустым'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'unique', 'targetClass' => '\app\models\Profile', 'message' => 'Этот адрес уже используется.'],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	public function attributeLabels() {
		return [
			'id'		 => 'ID',
			'user_id'	 => 'User ID',
			'firm'		 => 'Firm',
			'email'		 => 'Email',
		];
	}

	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public static function create($id,$email,$firm) {
		$user_data			 = new self();
		$user_data->loadDefaultValues();
		$user_data->user_id	 = $id;
		$user_data->email	 = $email;
		$user_data->firm	 = $firm;
		return $user_data->save();
	}

}