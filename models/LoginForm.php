<?php

namespace app\models;

use Yii;
use yii\base\Model;

class LoginForm extends Model {

	public $login;
	public $password;
	public $rememberMe = true;
	protected $_user;

	public function rules() {
		return [
			// username and password are both required
			[['login', 'password'], 'required'],
			// rememberMe must be a boolean value
			['rememberMe', 'boolean'],
			// password is validated by validatePassword()
			['password', 'validatePassword'],
		];
	}

	public function attributeLabels() {
		return [
			'login' => Yii::t('app', 'Логин'),
			'password' => Yii::t('app', 'Пароль'),
			'rememberMe' => Yii::t('app', 'Запомнить меня'),
		];
	}

	public function validatePassword($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if (!$user || !$user->validatePassword($this->password)) {
				$this->addError($attribute, 'Неверный логин или пароль.');
			}
		}
	}

	public function login() {
		if ($this->validate()) {
			return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
		}
		return false;
	}
	public function getUser() {
		if (!$this->_user) {
			$this->_user = User::findOne(['login' => $this->login]);
		}

		return $this->_user;
	}
}