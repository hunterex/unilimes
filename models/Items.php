<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 *
 * @property Categories $category
 */
class Items extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'items';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['category_id', 'name'], 'required'],
			[['category_id'], 'integer'],
			[['name'], 'string', 'max' => 64],
			[['category_id'], 'unique'],
			[['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'			 => '№ п/п',
			'category_id'	 => '№ категории',
			'name'			 => 'Наименование товара',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory() {
		return $this->hasOne(Categories::className(), ['id' => 'category_id']);
	}

}