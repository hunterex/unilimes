<?php

use yii\db\Migration;

/**
 * Class m200121_152749_nre_table_user
 */
class m200121_152749_create_table_user_profile extends Migration {

	public function safeDown() {
		echo "create_table_user cannot be reverted.\n";
		return false;
	}

	// Use up()/down() to run migration code without a transaction.
	public function safeUp() {

		$this->createTable('user', [
			'id'					 => $this->primaryKey(),
			'login'					 => $this->string(64)->notNull()->unique(),
			'pass'					 => $this->string(512)->notNull(),
			'autkKey'				 => $this->string(128),
			'accessToken'			 => $this->string(128),
			'password_reset_token'	 => $this->string(128),
		]);

		$this->createIndex(
		'idx-login', 'user', 'login', true
		);

		$this->createTable('profile', [
			'id'		 => $this->primaryKey(),
			'user_id'	 => $this->integer()->notNull(),
			'firm'		 => $this->string(64)->notNull(),
			'email'		 => $this->string(64)->notNull()->unique(),
		]);

		$this->createIndex(
		'idx-user_id-firm', 'profile', ['user_id', 'firm'], true
		);

		// add foreign key for table `post`
		$this->addForeignKey(
		'fk-user_id', 'profile', 'user_id', 'user', 'id', 'CASCADE'
		);
	}

}