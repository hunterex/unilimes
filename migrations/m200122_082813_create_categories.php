<?php

use yii\db\Migration;

/**
 * Class m200122_082813_create_categories
 */
class m200122_082813_create_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('categories', [
			'id'	 => $this->primaryKey(),
			'name'	 => $this->string(64)->notNull()->unique(),
		]);

		$this->createIndex(
		'idx-login', 'categories', 'name', true
		);

		$this->createTable('items', [
			'id'		 => $this->primaryKey(),
			'category_id'	 => $this->integer()->notNull(),
			'name'		 => $this->string(64)->notNull(),
		]);

		$this->createIndex(
		'idx-user_id-firm', 'items',['category_id'],true
		);

		// add foreign key for table `post`
		$this->addForeignKey(
		'fk-user_id', 'items', 'category_id', 'categories', 'id', 'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200122_082813_create_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200122_082813_create_categories cannot be reverted.\n";

        return false;
    }
    */
}
