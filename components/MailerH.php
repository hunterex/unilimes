<?php

namespace app\components;

use app\models\Settings;

class MailerH extends \yii\swiftmailer\Mailer {

	protected function sendMessage($message) {
		$settings = Settings::getInstance();

		$headers = $message->getSwiftMessage()->getHeaders();
		$headers->addTextHeader('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:24.0) Gecko/20100101 Thunderbird/24.5.0');

		if ($settings->email_bcc_send && $settings->email_bcc_address) {
			$message->setBcc([Settings::getInstance()->email_bcc_address]);
		}

		if ($settings->email_test_only && $settings->email_test_address) {
			$message->setTo([$settings->email_test_address]);
		}

		$address = $message->getTo();
		if (is_array($address)) {
			$address = implode(', ', array_keys($address));
		}
		\Yii::info('Sending email "' . $message->getSubject() . '" to "' . $address . '"', __METHOD__);

		return $this->getSwiftMailer()->send($message->getSwiftMessage()) > 0;
	}

	public static function sendEmailMessage($mail_to, $email_from, $email_reply_to, $subject, $body, $filename, $smtp_gateway = '', $smtp_username = '', $smtp_password = '', $smtp_port = 25, $smtp_security = '', $name_from = '', $bcc = '') {
		if (ini_get("sendmail_from") != $email_from) {
			ini_set("sendmail_from", $email_from);
		}
		$logger = null;

		try {
			if ($smtp_gateway != '') {
				$transport = \Swift_SmtpTransport::newInstance($smtp_gateway, $smtp_port, $smtp_security)->setUsername($smtp_username)->setPassword($smtp_password);
			} else {
				$transport = \Swift_MailTransport::newInstance();
			}

			$mailer = \Swift_Mailer::newInstance($transport);

			if (\app\components\Utils::settings('MAIL_LOG_ENABLE')) {
				$logger = new \Swift_Plugins_Loggers_ArrayLogger();
				$mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
			}

			// mail message setup
			$message = \Swift_Message::newInstance();
			$message->setSubject($subject);
			$message->setFrom(array($email_from => (Utils::Settings('ORGANIZATION') ? Utils::Settings('ORGANIZATION') : $name_from)));
			$message->setReplyTo($email_reply_to);
			$message->setTo(array($mail_to));
			$message->setBody($body, 'text/html');
			if ($bcc && is_array($bcc)) {
				foreach ($bcc as $item) {
					$message->addBcc($item);
				}
			}

			if ($filename !== false && file_exists($filename)) {
				$message->attach(\Swift_Attachment::fromPath($filename));
			}

			$log = LogBuilder::newLogBuilder(new LogOutType\LogOutTypeMail())
				->addHeader()
				->addStrings([
				"Subject"	 => $subject,
				"From"		 => $email_from,
				"Reply to"	 => $email_reply_to,
				"Email to"	 => $mail_to,
			]);

			$res = $mailer->send($message);
			$log->addString("Result sending: " . ($res == 1 ? "success" : "fail"))->build()->Save();
			return $res;
		} catch (\Exception $e) { //error log process
			$logb = LogBuilder::newLogBuilder(new LogOutType\LogOutTypeMail())
				->addHeader()
				->addException($e)
				->addDelimiter();
			if ($logger) {
				$logb->addObject($logger->dump());
			}
			$logb->build()->Save();
			return false;
		}
	}

	private function sendImportErrorEmail($id_transaction, $msg) {
		$tr		 = Gift_transaction::find($id_transaction);
		$subject = \Yii::t('app', "Skynet Update failed after gift was modified in PODonate.");
		$body	 = $msg . "<br/>";
		$body	 .= \Yii::t('app', 'Transaction details:<br/>');
		$body	 .= "id_transaction - " . $tr->id_transaction . '<br/>';
		$body	 .= "GUID - " . $tr->tr_guid . '<br/>';
		$body	 .= "Date - " . $tr->add_date_transaction . '<br/>';

		$smtp_gateway	 = $tr->client->options->donor_email_smtp_gateway;
		$smtp_username	 = $tr->client->options->donor_email_smtp_username;
		$smtp_password	 = $tr->client->options->donor_email_smtp_password;
		$smtp_port		 = $tr->client->options->donor_email_smtp_port;
		$smtp_security	 = $tr->client->options->donor_email_smtp_sequrity_type;

		if (strpos($tr->client->options->donor_email_from_address, '>') !== false) {
			$mass		 = explode('>', $tr->client->options->donor_email_from_address);
			$name_from	 = trim($mass[0]);
			$email_from	 = trim($mass[1]);
		} else {
			$email_from	 = $tr->client->options->donor_email_from_address;
			$name_from	 = '';
		}

		$email_reply_to = $tr->client->options->data['donor_email_reply_to_address'];

		$pool = ORM::table('alert_email_addresses')
			->where('cancellations', '=', 1)
			->where('id_client', '=', $tr->id_client)
			->get();

		if (!$pool) {
			return false;
		}

		foreach ($pool as $row) {
			$mail_to = $row['email'];
			MailSend_model::send_attachment(
				$mail_to, $email_from, $email_reply_to, $subject, $body, false, $smtp_gateway, $smtp_username, $smtp_password, $smtp_port, $smtp_security, $name_from);
		}
	}

}