<?php

namespace app\controllers;

use app\models\Categories;
use app\models\Items;
use app\models\ItemsSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ItemsController implements the CRUD actions for Items model.
 */
class ItemsController extends Controller {

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs'	 => [
				'class'		 => VerbFilter::className(),
				'actions'	 => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class'	 => AccessControl::className(),
				'rules'	 => [
					[
						'actions'	 => [
							'index',
							'view',
							'create',
							'update',
							'delete'
						],
						'allow'		 => true,
						'roles'		 => ['@'],
					]
				]
			]
		];
	}

	/**
	 * Lists all Items models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel	 = new ItemsSearch();
		$dataProvider	 = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel'		 => $searchModel,
			'dataProvider'		 => $dataProvider,
			'category_filter'	 => Categories::getCategory()
		]);
	}

	/**
	 * Displays a single Items model.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model'		 => $this->findModel($id),
			'category'	 => Categories::getCategory()
		]);
	}

	/**
	 * Creates a new Items model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Items();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		}

		return $this->render('create', [
			'model'		 => $model,
			'category'	 => Categories::getCategory()
		]);
	}

	/**
	 * Updates an existing Items model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		}

		return $this->render('update', [
			'model'		 => $model,
			'category'	 => Categories::getCategory()
		]);
	}

	/**
	 * Deletes an existing Items model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Items model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Items the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Items::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}

}