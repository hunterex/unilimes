<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class SiteController extends Controller {

	public
	function behaviors() {
		return [
			'access' => [
				'class'	 => AccessControl::className(),
				'only'	 => ['logout'],
				'rules'	 => [
					[
						'actions'	 => ['logout'],
						'allow'		 => true,
						'roles'		 => ['@'],
					],
				],
			],
			'verbs'	 => [
				'class'		 => VerbFilter::className(),
				'actions'	 => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions() {
		return [
			'error'		 => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha'	 => [
				'class'				 => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode'	 => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
	public function actionIndex() {
		return $this->render('index');
	}
	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			//return $this->goBack();
			return $this->redirect(Url::toRoute('items/index'));
		}

		$model->password = '';
		return $this->render('login', [
			'model' => $model,
		]);
	}
	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}
	public function actionContact() {
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');

			return $this->refresh();
		}
		return $this->render('contact', [
			'model' => $model,
		]);
	}
	public function actionAbout() {
		return $this->render('about');
	}
	public function actionSignup(){
		$model = new SignupForm();
		if (Yii::$app->request->isPost) {
			if ($model->load(Yii::$app->request->post())) {
				$user = $model->signup();
				if (empty($user)) {
					Yii::$app->session->setFlash('error', "Ошибка. Неправильные данные. Обратитесь, пожалуйста в техподдержку.");
					return $this->redirect(Url::toRoute("site/login"));
				}
				if(Yii::$app->getUser()->login($user)){
					return $this->redirect(Url::toRoute('catalog/index'));
				}
			}
		}
		return $this->render('signup', [
			'model' => $model
		]);
	}
	public function actionRequestPasswordReset() {
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				if ($model->sendEmail()) {
					Yii::$app->session->setFlash('success', 'Проверьте почту и следуйте инструкциям.');
					return $this->goHome();
				} else {
					Yii::$app->session->setFlash('error', 'Извините, мы не можем восстановить пароль для этого адреса.');
				}
			} catch (Exception $ex) {
				Yii::$app->session->setFlash('error', 'Извините, Произошла ошибка. Повторит, пожалуйста, позже.');
			}
		}

		return $this->render('requestPasswordResetToken', [
			'model' => $model,
		]);
	}
	public function actionResetPassword($token) {
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->session->setFlash('success', 'New password was saved.');
			return $this->goHome();
		}

		return $this->render('resetPassword', ['model' => $model,]);
	}

}