<?php
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

Hello,
Перейдите по ссылке для восстановления пароля:

<?= $resetLink ?>